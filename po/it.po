# Italian translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2019-09-15 11:50+0000\n"
"Last-Translator: Mike <miguel2000@livecom.it>\n"
"Language-Team: Italian <https://translate.ubports.com/projects/ubports/"
"calendar-app/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "Nuovo evento"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "Calendari"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "Indietro"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "Sincronizza"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "Sincronizzazione"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "Aggiungi calendario online"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "Impossibile deselezionare"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr ""
"Per poter creare nuovi eventi è necessario avere almeno un calendario "
"selezionato"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "OK"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "Nessun promemoria"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "All'evento"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 settimana"
msgstr[1] "%1 settimane"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 giorno"
msgstr[1] "%1 giorni"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 ora"
msgstr[1] "%1 ore"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minuti"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5 minuti"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10 minuti"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "15 minuti"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "30 minuti"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "1 ora"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "2 ore"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "1 giorno"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "2 giorni"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "1 settimana"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "2 settimane"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "Personalizzato"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "Agenda"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "Non ci sono calendari abilitati"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "Nessun evento imminente"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "Abilita calendari"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "nome evento non impostato"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "nessuna posizione"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "Mai"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "Dopo X eventi"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "Dopo la data"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "Nuovo evento"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 evento"
msgstr[1] "%1 eventi"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 evento intera giornata"
msgstr[1] "%1 eventi intera giornata"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "Modifica evento"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "Modificare solo l'evento «%1» o tutti gli eventi nella serie?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "Modifica serie"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "Modifica questo"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "Annulla"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "Nessun contatto"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "Cerca contatto"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "Elimina evento ricorrente"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "Elimina evento"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "Eliminare solo l'evento «%1» o tutti gli eventi della serie?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "Eliminare veramente l'evento «%1»?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "Elimina serie"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "Elimina questo"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "Elimina"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"L'applicazione Calendario accetta quattro argomenti: --starttime, --endtime, "
"--newevent e --eventid. Questi verranno gestiti dal sistema. Consultare il "
"codice sorgente per ulteriori informazioni sugli argomenti"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "Giorno"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "Settimana"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "Mese"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "Anno"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "Intera giornata"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "Impostazioni"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "Mostra numero settimana"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr "Mostra calendario cinese"

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr "Ore lavorative"

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "Promemoria predefinito"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "Calendario predefinito"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "Promemoria personalizzato"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "set"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "Ripeti"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "Ripeti:"

#: ../qml/EventRepetition.qml:233
msgid "Interval of recurrence"
msgstr "Intervallo di ripetizione"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "L'evento ricorrente termina"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "Ripeti"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "Data"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "Oggi"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "Anno %1"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr ""
"L'ora di fine dell'evento non può essere precedente a quella dell'inizio"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "Salva"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "Errore"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "OK"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "Da"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "A"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "Intera giornata"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "Dettagli evento"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "Nome dell'evento"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "Descrizione"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "Posizione"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "Calendario"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "Invitati"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "Aggiungi invitati"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "Promemoria"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "S %1"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "Modifica"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "Partecipano"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "Non partecipano"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "Forse"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "Nessuna risposta"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "Scegliere un account da creare."

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "Seleziona colore"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "Una volta"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "Ogni giorno"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "Nei giorni della settimana"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "Di %1, %2, %3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "Di %1 e %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "Ogni settimana"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "Ogni mese"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "Ogni anno"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 volta"
msgstr[1] "%1; %2 volte"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; fino al %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; ogni %1 giorni"

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; ogni %1 settimane"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; ogni %1 mesi"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; ogni %1 anni"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "Ogni settimana di %1"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "Un calendario per Ubuntu che si sincronizza con gli account online."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "calendario;evento;giorno;settimana;anno;appuntamento;riunione;"

#~ msgid "Show lunar calendar"
#~ msgstr "Mostra calendario lunare"
